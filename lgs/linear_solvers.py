"""
This module contains the implementation of solve_lu
which uses the lu decomposition to solve a linear system.

Date: 06/02/23
Author: L. Brumm; L. Kunath
"""

import numpy as np
import scipy as sp

# invalid-name for the mathematically inclined variable name
# pylint: disable=invalid-name

def solve_lu(p, l, u, b):
    """ Solves the linear system Ax = b via forward and backward substitution
    given the decomposition A = p * l * u.
    Parameters
    ----------
    p : numpy.ndarray
        permutation matrix of LU-decomposition
    l : numpy.ndarray
        lower triangular unit diagonal matrix of LU-decomposition
    u : numpy.ndarray
        upper triangular matrix of LU-decomposition
    b : numpy.ndarray
        vector of the right-hand-side of the linear system
    Returns
    -------
    x : numpy.ndarray
        solution of the linear system
    """
    # First solve ly = p.T * b by forward substitution
    y = sp.sparse.linalg.spsolve_triangular(l, p.T @ b)
    # Then solve ux = y by backward substitution
    return sp.sparse.linalg.spsolve_triangular(u, y, False)


if __name__ == "__main__":
    n = int(input("Please insert the dimension of the matrix: "))
    components = []
    for i in range(n):
        row = []
        while len(row) != n:
            row = list(map(float,
                input(f"Insert the {i}th row (space seperated): ").split()))
        components.append(row)
    matrix = np.array(components)
    inhomogenity = []
    while len(inhomogenity) != n:
        inhomogenity = list(map(float,
            input("Insert the right hand side: ").split()))
    inhomogenity = np.array(inhomogenity)
    print("Solving Ax = b for x")
    # For some reasone pylint is convinced, that sp.linalg.lu
    # only returns two variables, which is just not the case
    # as one can see from the doc string of the function and the
    # fact that the code works
    # pylint: disable=no-value-for-parameter
    print(solve_lu(*sp.linalg.lu(matrix), inhomogenity))
