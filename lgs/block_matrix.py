"""
Project: Projektpraktikum I WiSe 22/23
Title: block_matrix.py
Author: L. Brumm, L. Kunath
Date: 02/03/23

This module contains the class BlockMatrix which implements
utility functions to work with Laplacian matrix as they
appear in numerical solutions of the Poisson problem.
"""
from typing import Tuple, List
from functools import reduce
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

# invalid-name for mathematical inclined variable naming
# redefined-outer-name because we want to use the same names in the main block
# pylint: disable=invalid-name, redefined-outer-name


class BlockMatrix:
    """ Represents block matrices arising from finite difference approximations
    of the Laplace operator.

    Parameters
    ----------
    d : int
        Dimension of the space.
    n : int
        Number of intervals in each dimension.

    Attributes
    ----------
    d : int
        Dimension of the space.
    n : int
        Number of intervals in each dimension.

    Raises
    ------
    ValueError
        If d < 1 or n < 2.
    """

    def __init__(self, d, n):
        self.d = d
        self.n = n
        self.inner_points = n - 1
        self.matrix = None

    def get_sparse(self):
        """ Returns the block matrix as sparse matrix.

        Returns
        -------
        scipy.sparse.csr_matrix
            The block_matrix in a sparse data format.
        """
        tmp_matrix = sp.sparse.diags([[2 * self.d] * self.inner_points,
                      [-1] * self.inner_points,
                      [-1] * self.inner_points], [0, -1, 1], format="csr")
        for loop_index in range(2, self.d + 1):
            identity_submatrix = sp.eye(self.inner_points**(loop_index - 1))
            matrix_grid = np.array([None] * self.inner_points**2).\
                reshape((self.inner_points, self.inner_points))
            np.fill_diagonal(matrix_grid, tmp_matrix)
            # I could not find a nice way to set the secondary diagonals
            # So a loop is used...
            for i in range(0, self.inner_points - 1):
                matrix_grid[i + 1, i] = -identity_submatrix
                matrix_grid[i, i + 1] = -identity_submatrix
            tmp_matrix = sp.sparse.bmat(matrix_grid)
        self.matrix = sp.sparse.csr_matrix(tmp_matrix)
        return self.matrix

    def get_lu(self) -> Tuple[np.matrix, np.matrix, np.matrix]:
        """
        Returns the LU decomposition

        :returns: The P, L, U triple of dense matrices
        :raises ValueError: If the sparse matrix was not yet constructed
        """
        if self.matrix is None:
            _ = self.get_sparse()
        return sp.linalg.lu(sp.sparse.csr_matrix.todense(self.matrix))

    def eval_sparsity(self) -> Tuple[int, float]:
        """ Returns the absolute and relative numbers of non-zero elements of
        the matrix. The relative quantities are with respect to the total
        number of elements of the represented matrix.

        Returns
        -------
        int
            Number of non-zeros
        float
            Relative number of non-zeros

        Raises
        ------
            ValueError if matrix is None
        """
        if self.matrix is None:
            _ = self.get_sparse()
        nonzero = self.matrix.count_nonzero()
        return nonzero,\
            nonzero / reduce(lambda x, y: x * y, self.matrix.shape, 1)

    def eval_sparsity_lu(self):
        """ Returns the absolute and relative numbers of non-zero elements of
        the LU-Decomposition. The relative quantities are with respect to the
        total number of elements of the represented matrix.
        Returns
        -------
        int
        Number of non-zeros
        float
        Relative number of non-zeros

        Raises
        ------
            ValueError if matrix is None
        """
        if self.matrix is None:
            _ = self.get_sparse()
            # pylint: disable=unbalanced-tuple-unpacking
        _, lower, upper = self.get_lu()
        lower = np.asmatrix(lower)
        upper = np.asmatrix(upper)
        # By subtracting the identity from lower the ones on the diagonal of
        # lower are counted as non-zero elements. Because lower - identity
        # and upper are disjunct in the sense that they do not share non-zero
        # elements, the sum of the nonzero elements is equal to the nonzero
        # elements of the sum of the matrices.
        nonzero = np.count_nonzero(lower -
            np.identity(self.inner_points**self.d) + upper)
        return nonzero,\
            nonzero / reduce(lambda x, y: x * y, self.matrix.shape, 1)

    def get_cond(self):
        """ Computes the condition number of the represented matrix.
        Returns
        -------
        float
        condition number with respect to the infinity-norm
        """
        if self.matrix is None:
            _ = self.get_sparse()
        # As inverses of sparse matrices are not sparse in general
        # the inv function is only implemented on dense matrices
        inverse = sp.sparse.csr_matrix.todense(self.matrix)
        inverse = sp.linalg.inv(inverse)
        # Here the norm function for sparse matrices is used in hop
        # that this implementation is optimized for sparse matrices
        return sp.sparse.linalg.norm(self.matrix, ord=np.inf) *\
            sp.linalg.norm(inverse, ord=np.inf)


def plot_condition(axes: plt.Axes,
                   d: int,
                   ns: List[int],
                   reference=False) -> None:
    """Plot the condition of the laplace matrices for a given list of intervals

    Parameters
    ----------
    axes : numpy.axes
        The axes object to draw to
    param d : int
        The integer dimensionalaty of the problem
    param ns : [int]
        The list of integers specifying the discretisation steps
    param reference : bool
        Plot condition of hilbert matrices as well if set to True

    Returns
    -------
        None
    """
    conditions = []
    for n in ns:
        matrix = BlockMatrix(d, n)
        matrix.get_sparse()
        conditions.append(matrix.get_cond())

    axes.set_title("Condition w.r.t. $\\infty$-norm "
                   "as a function of N (the number of discretization steps)")
    axes.set_xlabel("N")
    axes.set_ylabel("Condition")
    axes.loglog(ns, conditions, label="Discretized Laplacian Matrix "
                                      f"{d}d-case")

    if reference:
        hilbert_matrix_conditions = np.zeros(len(ns))
        for i in range(len(ns)):  # pylint: disable=C0200
            hilbert_matrix = sp.linalg.hilbert(ns[i])
            inv_hilbert_matrix = sp.linalg.invhilbert(ns[i])
            hilbert_matrix_conditions[i] = sp.linalg.norm(hilbert_matrix,
                np.inf) * sp.linalg.norm(inv_hilbert_matrix, np.inf)
        Ns = (np.array(ns) - 1) ** d
        axes.loglog(Ns[1:], hilbert_matrix_conditions[1:], "--",
            label="Hilbert Matrices")
    axes.grid(True, which="both", ls="-", color='0.65')
    axes.legend()


def plot_number_of_not_zeros(axes: plt.Axes, d: int,  # pylint: disable=C0103
                             ns: List[int],
                             reference="",
                             absolute=True):
    """Plot the number of not zeros of the sparsity matrices

    Parameter:
    ---------
    axes : plt.Axes:
        The axes object to draw to
    d : int
        The integer dimensionality of the problem
    ns : [int]
        The list of integers specifying the discretisation steps
    reference : bool
        Plot condition of hilbert matrices as well if set to True
    absolute: bool
        A flag, either plot the absolute or relative sparsity

    Returns
    -------
        None
    """
    nonzero_sparse = []
    nonzero_lu = []
    # Set the absolute relative index according to the parameter
    # flag, it is used to either pick the absolute or relative
    # sparsity
    ari = 0 if absolute else 1
    for n in ns:
        matrix = BlockMatrix(d, n)
        matrix.get_sparse()
        nonzero_sparse.append(matrix.eval_sparsity()[ari])
        nonzero_lu.append(matrix.eval_sparsity_lu()[ari])
    axes.set_xlabel("N")
    axes.set_ylabel(["Absolute", "Relative"][ari] +
                    " Count")
    Ns = (np.array(ns) - 1) ** d
    axes.loglog(Ns, nonzero_sparse, label=f"{d}-dimensional sparse")
    axes.loglog(Ns, nonzero_lu, label=f"{d}-dimensional LU")

    if "1" in reference:
        axes.loglog(Ns, np.array(Ns), "--", label="n")
    if "2" in reference:
        axes.loglog(Ns, np.array(Ns) ** 2, "--", label="n**2")
    if "3" in reference:
        axes.loglog(Ns, np.array(Ns) ** 3, "--", label="n**3")
    if "5" in reference:
        axes.loglog(Ns, np.array(Ns) ** 5, "--", label="n**5")

    axes.legend()
    axes.grid(True, which="both", ls="-", color='0.65')


if __name__ == "__main__":
    dimensionality = int(input("Please insert the dimensionality "
                           "of your problem: "))
    n = int(input("Please insert number of intervals: "))
    block_matrix = BlockMatrix(dimensionality, n)
    _ = block_matrix.get_sparse()
    print("Sparsity: ", block_matrix.eval_sparsity())
    print("LU Sparsity: ", block_matrix.eval_sparsity_lu())
    plt.spy(block_matrix.get_sparse())
    plt.show()

    min_interval = int(input("Please insert the min interval: "))
    max_interval = int(input("Please insert the max interval: "))

    figure, axis = plt.subplots(1)
    axis.set_title("Condition over N w.r.t. infty-norm")
    ns = list(range(min_interval, max_interval))
    plot_condition(axis, dimensionality, ns)
    plt.show()

    figure, axis = plt.subplots(1)
    axis.set_title("Nonzero elements over N")
    ns = list(range(min_interval, max_interval))
    plot_number_of_not_zeros(axis, dimensionality, ns)
    plt.show()

    figure, axis = plt.subplots(1)
    axis.set_title("Relative Nonzero elements over N")
    ns = list(range(min_interval, max_interval))
    plot_number_of_not_zeros(axis, dimensionality, ns, "", False)
    plt.show()
