"""
Project: Projektpraktikum I WiSe 22/23
Title: poisson_problem.py
Author: L. Brumm, L. Kunath
Date: 06/02/23

This module contains the implementation of a numerical solution
of the poisson problem as well as related utility functions
"""

from typing import List
import numpy as np
from block_matrix import BlockMatrix
from linear_solvers import solve_lu

# invalid-name for the mathematically inclined variable names
# redefined-outer-name to use the same variable names in the main block
# We considered using enumeration
# pylint: disable=invalid-name,redefined-outer-name,consider-using-enumerate


def rhs(d, n, f):
    """ Computes the right-hand side vector ‘b‘ for a given function ‘f‘.
    Parameters
    ----------
    d : int
        Dimension of the space.
    n : int
        Number of intervals in each dimension.
    f : callable
        Function right-hand-side of Poisson problem. The calling signature is
        ‘f(x)‘. Here ‘x‘ is an array_like of ‘numpy‘. The return value
        is a scalar.
    Returns
    -------
        numpy.ndarray
        Vector to the right-hand-side f.
    Raises
    ------
        ValueError
            If d < 1 or n < 2
    """
    if d < 1 or n < 2:
        raise ValueError("d >= 1 and n >= 2 required")
    inner_points = n - 1
    discretized_input = np.zeros(inner_points**d)
    for i in range(0, len(discretized_input)):
        position = np.array(inv_idx(i + 1, d, n)) / n
        discretized_input[i] = f(position) / n**2
    return discretized_input


def idx(nx: List[int], n: int) -> int:
    """
    Calculates the number of an equation in the Poisson problem for
    a given discretization point.

    Parameters
    ----------
    nx: int
        The nxinate of the discretization point multiplied by n
    n: int
        The number of discretization n in each dimension

    Returns
    -------
        The index in the poisson problem
    """
    interiors = n - 1
    if len(nx) == 1:
        return nx[0]
    if len(nx) == 2:
        x = nx[0]
        y = nx[1] - 1
        return x + y * interiors
    if len(nx) == 3:
        x = nx[0]
        y = nx[1] - 1
        z = nx[2] - 1
        return x + y * interiors + z * interiors ** 2
    raise ValueError("Invalid dimension, expected 1, 2 or 3 dimension")


def inv_idx(m: int, d: int, n: int) -> List[int]:
    """ Calculates the coordinates of a discretization point for a
    given equation number of the Poisson problem.
    Parameters
    ----------
    m : int
        Number of an equation in the Poisson Problem
    d : int
        Dimension of the space.
    n : int
        Number of intervals in each dimension.

    Return
    ------
    list of int
    Coordinates of the corresponding discretization point, multiplied by n.
    """
    m = m - 1
    interiors = n - 1
    if m >= interiors ** d:
        raise ValueError(f"m > interiors**{d}")
    if d == 1:
        return [m + 1]
    if d == 2:
        y = m // interiors
        x = (m - y * interiors) % interiors
        return [x + 1, y + 1]
    if d == 3:
        z = m // (interiors ** 2)
        m = m - z * (interiors ** 2)
        y = m // interiors
        m = m - y * interiors
        x = m % interiors
        return [x + 1, y + 1, z + 1]
    raise ValueError("d has to be either 1, 2 or three")


def solve_poisson_problem(d, n, f):
    """
    Solve the poisson problem for a given inhomeginity using LU decomposition

    Parameters
    ----------
    d: int
        Dimensions of the problem
    n: int
        Number of interval in each dimension
    f: callable Int -> Float
        yielding the inhomogenity at a given position

    Returns
    -------
    A list of floats
        containing the flattened solution to the
        n-dimensional problem
    """
    discretized_inhomogenity = rhs(d, n, f)
    block_matrix = BlockMatrix(d, n)
    _ = block_matrix.get_sparse()
    # Again pylinnt is convinced get_lu would return two variables
    # pylint: disable=unbalanced-tuple-unpacking
    permutation, lower, upper = block_matrix.get_lu()
    return -np.array(solve_lu(permutation, lower,
        upper, discretized_inhomogenity))


def compute_error(d, n, hat_u, u):
    """ Computes the error of the numerical solution of the Poisson problem
    with respect to the infinity-norm.

    Parameters
    ----------
    d : int
        Dimension of the space
    n : int
        Number of intersections in each dimension
    hat_u : numpy.array of floats
        Finite difference approximation of the solution of the Poisson problem
        at the discretization points
    u : callable
        Solution of the Poisson problem
        The calling signature is ’u(x)’. Here ’x’ is an array_like of ’numpy’.
        The return value is a scalar.

    Returns
    -------
    float
        maximal absolute error at the discretization points
    """
    max_error = 0
    for i in range(len(hat_u)):
        position = np.array(inv_idx(i + 1, d, n)) / n
        error = abs(u(position) - hat_u[i])
        if error > max_error:
            max_error = error
    return max_error


def plot_errors(d, ns, inhomogenity, analytical_solution, axes):
    """
    Plot the error of the discretized solution to the Poisson problem

    Parameters
    ----------
    d: int
        Dimensions of the problem
    ns: [int]
        A list of intervals for which to calculate the problem
    inhomogenity: callable
        The callable real function specifying the righthand side of
        the problem
    analyitcal_solution: callable
        The callable real function specifying the
        analytical solution to the Poisson problem
    axes:
        The axes object to draw on
    """
    errors = np.zeros(len(ns))
    for i in range(len(errors)):
        n = ns[i]
        inner_points = n - 1
        print(inner_points)
        block_matrix = BlockMatrix(d, n)
        _ = block_matrix.get_sparse()
        numerical_solution = np.array(solve_poisson_problem(d,
            n, inhomogenity))
        errors[i] = compute_error(d, n,
            numerical_solution, analytical_solution)
    # It is requested to plot the errors w.r.t N which is defined
    # as (n - 1)**d
    inner_pointss = (np.array(ns) - 1) ** d
    axes.loglog(inner_pointss, errors, label=f"{d}d-case")


if __name__ == "__main__":
    d = 2
    n = 5
    print("Demonstation of inv_idx and idx for d={d} and n={n}")
    for i in range(1, 17):
        print(f"{i} -> {inv_idx(i, d, n)} -> {idx(inv_idx(i, d, n), n)}")
    print("Applying the function rhs on x -> exp(x) with d=1 and n=11")
    print(rhs(1, 11, np.exp))
