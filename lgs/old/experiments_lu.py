"""
Project: Projektpraktikum I WiSe 22/23
Title: linear_solvers.py
Author: L. Brumm, L. Kunath
Date: 13/12/22

This module contains all code required to reproduce the results of
our numerical experiments
"""
import numpy as np
import poisson_problem as pp
import block_matrix as bm


def analytical_solution(position, kappa=1.0):
    """
    Calculates the value of the analytical solution
    at a given position

    :param position: The argument of the function as a numpy array
                     of length 1, 2 or 3
    :param kappa: An optional integer parameter to the function
    :return: The float value of the analytical solution at $position
    """
    result = 1
    for component in position:
        result *= component * np.sin(kappa * np.pi * component)
    return result


def inhomogenity(position, kappa=1.0):
    """
    Calculates the value of the inhomogenity
    at a given position

    :param position: The argument of the function as a numpy array
                     of length 1, 2 or 3
    :param kappa: An optional integer parameter to the function
    :return: The float value of the inhomogenity at $position
    """
    if len(position) == 1:
        x = position[0]  # pylint: disable=C0103
        return 2 * kappa * np.pi * np.cos(kappa * np.pi * x) -\
            kappa**2 * np.pi**2 * x * np.sin(kappa * np.pi * x)
    if len(position) == 2:
        x = position[0]  # pylint: disable=C0103
        y = position[1]  # pylint: disable=C0103
        ddx = 2 * kappa * np.pi * np.cos(kappa * np.pi * x) -\
            kappa**2 * np.pi**2 * x * np.sin(kappa * np.pi * x)
        ddy = 2 * kappa * np.pi * np.cos(kappa * np.pi * y) -\
            kappa**2 * np.pi**2 * y * np.sin(kappa * np.pi * y)
        return \
            ddx * y * np.sin(kappa * np.pi * y) + \
            ddy * x * np.sin(kappa * np.pi * x)
    if len(position) == 3:
        x = position[0]  # pylint: disable=C0103
        y = position[1]  # pylint: disable=C0103
        z = position[2]  # pylint: disable=C0103
        w = kappa * np.pi  # pylint: disable=C0103
        ddx = 2 * w * np.cos(w * x) -\
            kappa**2 * np.pi**2 * x * np.sin(w * x)
        ddy = 2 * w * np.cos(w * y) -\
            kappa**2 * np.pi**2 * y * np.sin(w * y)
        ddz = 2 * w * np.cos(w * z) - \
            kappa**2 * np.pi**2 * z * np.sin(w * z)
        return \
            ddx * y * np.sin(w * y) * z * np.sin(w * z) +\
            ddy * x * np.sin(w * x) * z * np.sin(w * z) +\
            ddz * x * np.sin(w * x) * y * np.sin(w * y)
    raise ValueError("position has to be a 1-, 2- or 3-dimensional vector")


def plot_condition():
    """
    Plot the condition of the discretized laplacians and the hilbert
    matrices as a reference
    """
    _, axes = plt.subplots(1)
    bm.plot_condition(axes, 1, list(range(1, 15)), True)
    bm.plot_condition(axes, 2, list(range(1, 15)), False)
    bm.plot_condition(axes, 3, list(range(1, 15)), False)
    plt.show()


def plot_poisson_solution():
    """
    Plot the solution of poissonproblem for dimensions 1 and 2 for
    different stepsizes
    """
    figure, axes = plt.subplots(2, 2)
    figure.suptitle("Solution of poisson problem for dimension 1")
    figure.tight_layout(pad=2.5)

    discretisation = np.linspace(0, 1, 1024)
    solution_array = np.zeros(len(discretisation))
    for i in range(len(discretisation)):
        solution_array[i] = analytical_solution([i / len(discretisation)])

    for i in range(4):
        X = np.linspace(0, 1, 2**(i + 2))  # pylint: disable=C0103
        poisson_solution = pp.solve_poisson_problem(1, 2**(i + 2),
            inhomogenity)
        error = round(pp.compute_error(1, 2**(i + 2), poisson_solution,
            analytical_solution), 3)

        pic_now = [(0, 0), (0, 1), (1, 0), (1, 1)]
        axes[pic_now[i]].plot(X, poisson_solution)
        axes[pic_now[i]].plot(discretisation, solution_array, "--")
        axes[pic_now[i]].set_title(f"n={2**(i+2)}, error={error}")

    plt.show()

    figure, axes = plt.subplots(2, 2, subplot_kw={"projection": "3d"})
    figure.suptitle("Solution of poisson problem for dimension 2")
    plt.subplots_adjust(hspace=0.5)

    for i in range(4):
        X = np.linspace(0, 1, 2**(i + 2))  # pylint: disable=C0103
        Y = np.linspace(0, 1, 2**(i + 2))  # pylint: disable=C0103
        X, Y = np.meshgrid(X, Y)  # pylint: disable=C0103
        poisson_solution = np.array(pp.solve_poisson_problem(2, 2**(i + 2),
            inhomogenity))
        error = round(pp.compute_error(2, 2**(i + 2), poisson_solution,
            analytical_solution), 4)
        poisson_solution = poisson_solution.reshape((2**(i + 2), 2**(i + 2)))

        pic_now = [(0, 0), (0, 1), (1, 0), (1, 1)]
        axes[pic_now[i]].plot_surface(X, Y, poisson_solution, cmap=cm.coolwarm)
        axes[pic_now[i]].set_title(f"n={2**(i+2)}, error={error}",
            loc="left")

    plt.show()


def plot_2d_case():
    """
    Plot the numerical solution of the 2d case
    """
    figure, axes = plt.subplots(2, 2, subplot_kw={"projection": "3d"})
    figure.suptitle("Comparison of numerical and analytical solutions")
    X = np.linspace(0, 1, 4)  # pylint: disable=C0103
    Y = np.linspace(0, 1, 4)  # pylint: disable=C0103
    X, Y = np.meshgrid(X, Y)  # pylint: disable=C0103
    numerical_solution_n4 = np.array(pp.solve_poisson_problem(
        2, 4, inhomogenity)).reshape((4, 4))
    axes[0, 0].plot_surface(X, Y, numerical_solution_n4, cmap=cm.coolwarm)
    axes[0, 0].set_title("Numerical Solution n=4")

    X = np.linspace(0, 1, 11)  # pylint: disable=C0103
    Y = np.linspace(0, 1, 11)  # pylint: disable=C0103
    X, Y = np.meshgrid(X, Y)  # pylint: disable=C0103
    numerical_solution_n11 = np.array(pp.solve_poisson_problem(
        2, 11, inhomogenity)).reshape((11, 11))
    axes[0, 1].plot_surface(X, Y, numerical_solution_n11, cmap=cm.coolwarm)
    axes[0, 1].set_title("Numerical Solution n=11")

    X = np.linspace(0, 1, 4)  # pylint: disable=C0103
    Y = np.linspace(0, 1, 4)  # pylint: disable=C0103
    X, Y = np.meshgrid(X, Y)  # pylint: disable=C0103
    Z = np.zeros(4**2)  # pylint: disable=C0103
    for i in range(len(Z)):  # pylint: disable=C0200
        Z[i] = analytical_solution(np.array(pp.inv_idx(i, 2, 4)) / 4)
    axes[1, 0].plot_surface(X, Y, Z.reshape((4, 4)),
                         cmap=cm.coolwarm)
    axes[1, 0].set_title("Analytical Solution n=4")

    X = np.linspace(0, 1, 11)  # pylint: disable=C0103
    Y = np.linspace(0, 1, 11)  # pylint: disable=C0103
    X, Y = np.meshgrid(X, Y)  # pylint: disable=C0103
    Z = np.zeros(11**2)  # pylint: disable=C0103
    for i in range(len(Z)):  # pylint: disable=C0200
        Z[i] = analytical_solution(np.array(pp.inv_idx(i, 2, 11)) / 11)
    axes[1, 1].plot_surface(X, Y, Z.reshape((11, 11)),
                         cmap=cm.coolwarm)
    axes[1, 1].set_title("Analytical Solution n=11")
    plt.show()


def plot_errors():
    """
    Plot the convergence of the numerical error for all three
    dimensionalities
    """
    figure, axes = plt.subplots(1)
    figure.suptitle("The error convergence for different dimensionalities")
    stepss = list(range(2, 20))
    pp.plot_errors(1, stepss,
                   inhomogenity, analytical_solution, axes)
    pp.plot_errors(2, stepss,
                   inhomogenity, analytical_solution, axes)
    pp.plot_errors(3, stepss,
                   inhomogenity, analytical_solution, axes)
    axes.grid(True, which="both", ls="-", color="0.65")
    axes.legend()
    plt.show()


def plot_sparsity():
    """
    Plot the sparsity of the laplacian matrix and its LU-decomposition
    """
    figure, axes = plt.subplots(3)
    figure.suptitle("Total Count of nonzero elements")
    bm.plot_number_of_not_zeros(axes[0], 1, list(range(1, 100)), "1")
    bm.plot_number_of_not_zeros(axes[1], 2, list(range(1, 20)), "23")
    bm.plot_number_of_not_zeros(axes[2], 3, list(range(1, 10)), "35")
    plt.show()

    figure, axes = plt.subplots(1)
    figure.suptitle("Total Count of nonzero elements combined plot")
    bm.plot_number_of_not_zeros(axes, 1,
        list(range(1, 100)), absolute=False)
    bm.plot_number_of_not_zeros(axes, 2,
        list(range(1, 20)), absolute=False)
    bm.plot_number_of_not_zeros(axes, 3,
        list(range(1, 15)), absolute=False)
    axes.loglog(list(range(1, 100)), [0.5 for x in range(1, 100)], "--")
    plt.show()


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from matplotlib import cm

    while True:
        try:
            print("0. Plot 2d numerical and analytical solution (n=4, n=11)")
            print("1. Plot error of numerical solution (may take few minutes")
            print("2. Plot Discretized Laplacian Matrix Conditioning Number")
            print("3. Plot Sparsity of discretized Laplacian Matrix and "
                  "their\n\tLU-Decomposition")
            print("4. Plot 1d and 2d numerical solutions")
            choice = int(input("Choose wisely: "))
            if choice not in [0, 1, 2, 3, 4]:
                raise ValueError
            break
        except ValueError:
            print("I sad wisely!")
    [plot_2d_case, plot_errors, plot_condition,
     plot_sparsity, plot_poisson_solution][choice]()
