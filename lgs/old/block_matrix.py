"""
Project: Projektpraktikum I WiSe 22/23
Title: block_matrix.py
Author: L. Brumm, L. Kunath
Date: 16.12.22

This module contains the class BlockMatrix which implements
utility functions to work with Laplacian matrix as they
appear in numerical solutions of the Poisson problem.
"""
from functools import reduce
from typing import Tuple, List
from scipy import linalg
from scipy import sparse as sp
from matplotlib.axes import Axes
import numpy as np


# A cache of already calculated matrix attributes
# entries are stored in the form (d, steps) -> sparse
CACHE = {}


def frac(nominator: float, denominator: float) -> float:
    """
    Calculates the fraction of two floats

    :param nominator: Nominator (float)
    :param denominator: Denominator (float)
    :return: nominator / denominator (float)
    :raises ZeroDivisionError: if b is zero
    """
    return nominator / denominator


class BlockMatrix:
    """ Represents block matrices arising from finite difference approximations
    of the Laplace operator.

    Parameters
    ----------
    dimension : int
        Dimension of the space.
    steps : int
        Number of intervals in each dimension.

    Attributes
    ----------
    d : int
        Dimension of the space.
    n : int
        Number of intervals in each dimension.

    Raises
    ------
    ValueError
        If d < 1 or n < 2.
    """

    def __init__(self, dimension: int, steps: int, fmt="csr"):
        self.dimension = dimension
        self.steps = steps

        if (self.dimension, self.steps) in CACHE.keys():
            self.matrix = CACHE[(self.dimension, self.steps)]

        tmp_matrix = sp.diags([[2 * self.dimension] * self.steps,
                      [-1] * self.steps,
                      [-1] * self.steps], [0, -1, 1], format=fmt)
        for loop_index in range(2, self.dimension + 1):
            identity_submatrix = sp.eye(self.steps**(loop_index - 1))
            matrix_grid = np.array([None] * self.steps**2).\
                reshape((self.steps, self.steps))
            np.fill_diagonal(matrix_grid, tmp_matrix)
            # I could not find a nice way to set the secondary diagonals
            # So a loop is used...
            for i in range(0, self.steps - 1):
                matrix_grid[i + 1, i] = -identity_submatrix
                matrix_grid[i, i + 1] = -identity_submatrix
            tmp_matrix = sp.bmat(matrix_grid)
        self.matrix = sp.csr_matrix(tmp_matrix)
        CACHE[(self.dimension, self.steps)] = self.matrix

    def get_sparse(self):
        """
        Return the sparse matrix
        """
        return self.matrix

    def eval_sparsity(self):
        """
        Returns the absolute and relative numbers of non-zero elements of
        the matrix. The relative quantities are with respect to the total
        number of elements of the represented matrix.
        :return: A tuple (integer, float) storing the absolute and relative
                 number of non-zero elements
        :raises ValueError: If the sparse matrix was not yet constructed
        """
        if self.matrix is None:
            raise ValueError("sparse matrix is None, "
                             "did you forget to call 'get_sparse'?")
        nonzero = self.matrix.count_nonzero()
        return nonzero,\
            nonzero / reduce(lambda x, y: x * y, self.matrix.shape, 1)

    def get_lu(self) -> Tuple[np.matrix, np.matrix, np.matrix]:
        """
        Returns the LU decomposition

        :returns: The P, L, U triple of dense matrices
        :raises ValueError: If the sparse matrix was not yet constructed
        """
        if self.matrix is None:
            raise ValueError("sparse matrix is None, "
                             "did you forget to call 'get_sparse'?")
        return linalg.lu(sp.csr_matrix.todense(self.matrix))

    def eval_sparsity_lu(self) -> Tuple[int, float]:
        """
        Calculates the absolute and relative sparsity of the LU decomposition

        :returns: (absolute_sparsity, relative_sparsity of type
                  (int, float)
        :raises ValueError: If the sparse matrix was not yet constructed
        """
        if self.matrix is None:
            raise ValueError("sparse matrix is None, "
                             "did you forget to call 'get_sparse'?")
        _, left, upper = self.get_lu()  # pylint: disable=W0632
        left = np.asmatrix(left)
        upper = np.asmatrix(upper)
        combined = left + upper

        nonzero = 0
        for x in range(combined.shape[0]):  # pylint: disable=invalid-name
            for y in range(combined.shape[1]):  # pylint: disable=invalid-name
                if combined[x, y] != 0:
                    nonzero += 1
        return nonzero,\
            nonzero / reduce(lambda x, y: x * y, self.matrix.shape, 1)

    def get_cond(self) -> float:
        """
        Calculates the matrix condition w.r.t. the infinity norm
        (Caution, this includes inverting the matrix and may take a while)

        :returns: The condition w.r.t the infinity norm as float
        :raises ValueError: If the sparse matrix was not yet constructed
        """
        if self.matrix is None:
            raise ValueError("sparse matrix is None, "
                             "did you forget to call 'get_sparse'?")
        inverse = sp.csr_matrix.todense(self.matrix)
        inverse = linalg.inv(inverse)
        return sp.linalg.norm(self.matrix, ord=np.inf) *\
            linalg.norm(inverse, ord=np.inf)


def plot_condition(axes: Axes, d: int,  # pylint: disable=C0103
                   ns: List[int],
                   reference=False) -> None:
    """Plot the condition of the laplace matrices for a given list of
    discretisation steps

    Parameters
    ----------
    axes : numpy.axes
        The axes object to draw to
    param d : int
        The integer dimensionalaty of the problem
    param ns : [int]
        The list of integers specifying the discretisation steps
    param reference : bool
        Plot condition of hilbert matrices as well if set to True

    Returns
    -------
        None
    """
    conditions = []
    for n in ns:
        matrix = BlockMatrix(d, n)
        matrix.get_sparse()
        conditions.append(matrix.get_cond())

    axes.set_title("Condition w.r.t. $\\infty$-norm "
                   "as a function of N (the number of discretization steps)")
    axes.set_xlabel("N")
    axes.set_ylabel("Condition")
    axes.loglog(ns, conditions, label="Discretized Laplacian Matrix "
                                      f"{d}d-case")

    if reference:
        hilbert_matrix_conditions = np.zeros(len(ns))
        for i in range(len(ns)):  # pylint: disable=C0200
            hilbert_matrix = linalg.hilbert(ns[i])
            inv_hilbert_matrix = linalg.invhilbert(ns[i])
            hilbert_matrix_conditions[i] = linalg.norm(hilbert_matrix,
                np.inf) * linalg.norm(inv_hilbert_matrix, np.inf)
        axes.loglog(ns, hilbert_matrix_conditions, "--",
            label="Hilbert Matrices")
    axes.grid(True, which="both", ls="-", color='0.65')
    axes.legend()


def plot_number_of_not_zeros(axes: Axes, d: int,  # pylint: disable=C0103
                             stepss: List[int],
                             reference="",
                             absolute=True):
    """
    Plot the number of not zeros of the sparsity matrices

    :param axes: The axes object to draw to
    :param d: The integer dimensionality of the problem
    :param stepss: The list of integers specifying the discretisation steps
    :param reference: Plot condition of hilbert matrices as well if set to True
    :param absolute: A (boolean) flag, either plot the absolute or relative
                     sparsity
    :return None
    """
    nonzero_sparse = []
    nonzero_lu = []
    # Set the absolute relative index according to the parameter
    # flag, it is used to either pick the absolute of relative
    # sparsity
    ari = 0 if absolute else 1
    for steps in stepss:
        matrix = BlockMatrix(d, steps)
        matrix.get_sparse()
        nonzero_sparse.append(matrix.eval_sparsity()[ari])
        nonzero_lu.append(matrix.eval_sparsity_lu()[ari])
    axes.set_xlabel("N")
    axes.set_ylabel("Count")

    axes.loglog(stepss, nonzero_sparse, label=f"{d}-dimensional sparse")
    axes.loglog(stepss, nonzero_lu, label=f"{d}-dimensional LU")

    if "1" in reference:
        axes.loglog(stepss, np.array(stepss), "--", label="n")
    if "2" in reference:
        axes.loglog(stepss, np.array(stepss) ** 2, "--", label="n**2")
    if "3" in reference:
        axes.loglog(stepss, np.array(stepss) ** 3, "--", label="n**3")
    if "5" in reference:
        axes.loglog(stepss, np.array(stepss) ** 5, "--", label="n**5")

    axes.legend()
    axes.grid(True, which="both", ls="-", color='0.65')


if __name__ == "__main__":
    # import matplotlib.pyplot as plt
    # block_matrix = BlockMatrix(2, 10)
    # plt.spy(block_matrix.get_sparse())
    # plt.show()
    block_matrix = BlockMatrix(1, 8)
    print(block_matrix.get_sparse())
