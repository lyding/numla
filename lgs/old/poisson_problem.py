"""
Project: Projektpraktikum I WiSe 22/23
Title: block_matrix.py
Author: L. Brumm, L. Kunath
Date: 16.12.22

This module contains functions to solve the Poisson problem numerically
as well as functions to calculate and plot the error of the approximation
"""
from typing import List
import numpy as np
from linear_solvers import solve_lu
from block_matrix import BlockMatrix

# pylint: disable=W0621, C0200


def rhs(dimensions, steps, input_function):
    """
    Computes the right-hand side vector `b` for a given function `f`.

    :param dimensions: The integer number of dimensions
    :param steps: The integer number of discretization steps in each dimension
    :param input_function: A callable python function specifying the
                           inhomogenity of the Poisson problem
    :returns: The inhomogenity of the poisson problem to which input_function
              would be the solution (numpy matrix)
    :raises ValueError: if d not equal to 1, 2 or 3
    """
    if dimensions not in [1, 2, 3] or steps < 2:
        raise ValueError
    discretized_laplacian = BlockMatrix(dimensions, steps)
    discretized_laplacian.get_sparse()
    discretized_input = np.zeros(steps**dimensions)
    for i in range(len(discretized_input)):
        position = inv_idx(i, dimensions, steps)
        discretized_input[i] = input_function(np.array(position))
    return discretized_laplacian.matrix @ discretized_input


def idx(coord: List[int], steps: int) -> int:
    """
    Calculates the number of an equation in the Poisson problem for
    a given discretization point.

    :param coord: The coordinate of the discretization point multiplied by n
    :param steps: The number of discretization steps in each dimension
    :return: The index in the poisson problem
    """
    result = 0
    for i in range(len(coord)):
        result += coord[i] * steps**i
    return result


def inv_idx(index: int, dimensions: int, steps: int) -> List[int]:
    """
    Calculates the coordinates of a discretization point for a
    given equation number of the Poisson problem.

    :param index: The index of the equation in the poisson problem
    :param dimensions: Number of dimensions
    :param steps: The number of discretization steps in each dimension
    :return: A list of integer containing the coords of the discretization
             point
    """
    coords = [0 for x in range(dimensions)]
    for dimension in range(dimensions):
        coords[dimensions - dimension - 1] = int(index //
            steps**(dimensions - dimension - 1))
        index = index - coords[dimensions - dimension - 1] *\
            steps**(dimensions - dimension - 1)
    return coords


def solve_poisson_problem(dimensions, steps, inhomogenity):
    """
    Solve the poisson problem for a given inhomeginity using LU decomposition

    :param dimensions: The integer dimensions of the problem
    :param steps: The integer number of discretization steps in each dimension
    :param inhomogenity: A python callable yielding the inhomogenity
                         at a given position [Integer] -> float
    :return: A list of float containing the flattened solution to the
             n-dimensional problem
    """
    discretized_inhomogenity = np.zeros(steps**dimensions)
    # First the inhomogenity has to be discretized one vector b
    for i in range(len(discretized_inhomogenity)):
        position = np.array(inv_idx(i, dimensions, steps)) / steps
        discretized_inhomogenity[i] = inhomogenity(position)
    block_matrix = BlockMatrix(dimensions, steps)
    _ = block_matrix.get_sparse()
    permutation, lower, upper = block_matrix.get_lu()  # pylint: disable=W0632
    return np.array(solve_lu(permutation, lower,
        upper, discretized_inhomogenity)) / -(steps**2)


def compute_error(dimensions, steps, approximated_values, solution):
    """
    Computes the maximal error of the numerical solution
    of the Poisson problem

    :param: dimensions: Dimension of space
    :param: steps: Number of discretization steps in each dimension
    :param: approximated_values: An array storing the numerical solution
    :param: solution: A python callable yielding the value of the exact
                      solution to the Poisson problem at a given
                      discretization step
    :return: The maximal difference between the approximated and exact solution
    """
    max_error = 0
    for i in range(len(approximated_values)):
        position = np.array(inv_idx(i, dimensions, steps)) / steps
        error = abs(solution(position) - approximated_values[i])
        if error > max_error:
            max_error = error
    return max_error


def plot_errors(dimensions, stepss, inhomogenity, analytical_solution, axes):
    """
    Plot the error of the discretized solution to the Poisson problem

    :param dimensions: The integer dimensions of the problem
    :param stepss: An array of number steps for which to calculate the problem
    :param inhomogenity: The callable real function specifying the right
                         hand side of the problem
    :param analyitcal_solution: The callable real function specifying the
                                analytical solution to the Poisson problem
    :param axes: The axes object to draw on
    """
    errors = np.zeros(len(stepss))
    for i in range(len(errors)):
        steps = stepss[i]
        discretized_inhomogenity = np.zeros(steps**dimensions)
        for j in range(len(discretized_inhomogenity)):
            position = np.array(inv_idx(j, dimensions, steps)) / steps
            discretized_inhomogenity[i] = inhomogenity(position)

        numerical_solution = np.array(solve_poisson_problem(dimensions,
            steps, inhomogenity))
        errors[i] = compute_error(dimensions, steps,
            numerical_solution, analytical_solution)
    axes.plot(stepss, errors, label=f"{dimensions}d-case")


if __name__ == "__main__":
    print("Demonstration that inv_idx is indeed the inverse idx function:")
    indices = list(range(25))
    positions = []
    print("Indices:", indices)
    for i in indices:
        positions.append(inv_idx(i, 2, 5))
    print("Positions:", positions)
    indices = []
    for i in range(25):
        indices.append(idx(positions[i], 5))
    print("Indices:", indices)
