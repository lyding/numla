"""
Project: Projektpraktikum I WiSe 22/23
Title: derivative_approximation.py
Author: L. Brumm, L. Kunath
Date: 30/11/22

This module contains the class FiniteDifference,
which encapsulates the first and second order finite differentiaion
of a float->float like function as well as utility functions
used in the exercises.py file.
"""
from typing import Callable as Cal
from typing import Optional as Opt
from typing import Tuple, List
from matplotlib.axes import Axes
import numpy as np
import matplotlib.pyplot as plt

Realf = Cal[[float], float]
ORealf = Opt[Cal[[float], float]]


def frac(nominator: float, denominator: float) -> float:
    """
    Calculates the fraction of two floats

    :param nominator: Nominator (float)
    :param denominator: Denominator (float)
    :return: nominator / denominator (float)
    :raises ZeroDivisionError: if b is zero
    """
    return nominator / denominator


def frompyfunc11(pyfunc: Realf):
    """
    Convert a python function with one input parameter and one
    output value into a nump ufunc

    :param pyfunc: The function to convert (float->float)
    :return: pyfunc as a ufunc (float->float)
    """
    return np.frompyfunc(pyfunc, 1, 1)


class FiniteDifference:
    """
    Represents the first and second order finite difference approximation
    of a function and allows for a computation of error to the exact
    derivatives (if provided).
    """
    def __init__(self, stepwidth: float, func: Realf,
                 d_func: ORealf = None, dd_func: ORealf = None):
        """
        :param stepwidth: Stepwidth of the numerical differentiation (float)
        :param func: The function to differentiate numerically (float->float)
        :param d_func: The first derivative f' (float->float)
        :param dd_func: The seocond derivative f'' (float->float)
        """
        self.stepwidth = stepwidth
        self.func = func
        self.d_func = d_func
        self.dd_func = dd_func

    def compute_dh_f(self) -> Realf:
        """
        :return: A function calculating the first derivative at a given x
                 (float->float)
        """
        def inner(x: float) -> float:  # pylint: disable=invalid-name
            """
            Calculates the first derivative at x numerically with
            the provided stepwidth

            :param x: The point of interest (float)
            :return: The first derivative at x (float)
            """
            stepwidth = self.stepwidth
            return frac(self.func(x + stepwidth) - self.func(x), stepwidth)
        return inner

    def compute_ddh_f(self) -> Realf:
        """
        :return: A function calculating the second derivative of f at a given x
                 (float->float)
        """
        def inner(x: float) -> float:  # pylint: disable=invalid-name
            """
            Calculates the second derivative at x numerically with stepwidth h

            :param x: The point of interest (float)
            :return: The second derivative at x (float)
            """
            stepwidth = self.stepwidth
            func = self.func
            return frac(func(x + stepwidth) - 2 * func(x)
                        + func(x - stepwidth), stepwidth**2)
        return inner

    def compute_errors(self, left: float,
                       right: float,
                       steps: int) -> Tuple[float, float]:
        """
        Computes the errors e_1 and e_2 for a given intervall

        :param left: The left intervall boundary (float)
        :param right: The right intervall boundary (float)
        :param steps: The number of discretization steps (int)
        :return: A tuple of errors e_1 and e_2 (float, float)
        :raises ValueError: if no analytical derivitatives where provided
        """
        if self.d_func is None or self.dd_func is None:
            raise ValueError
        x_s = np.linspace(left, right, steps + 1)
        e_1 = np.abs(frompyfunc11(self.d_func)(x_s) -
                    frompyfunc11(self.compute_dh_f())(x_s))
        e_2 = np.abs(frompyfunc11(self.dd_func)(x_s) -
                    frompyfunc11(self.compute_ddh_f())(x_s))
        return max(e_1), max(e_2)


def plot_functions(finite_difference: FiniteDifference,
                   left: float, right: float, steps: int) -> None:
    """
    A helper function plotting the function and all its derivatives
    of a given finite_difference object in an intervall [a, b] with
    p discretization steps

    :param finite_difference: functions to plot (FiniteDifference)
    :param left: The left intervall bounbary (float)
    :param right: The right intervall bounbary (float)
    :param steps: The number of discretization steps (int)
    :return: None
    """
    x_s = np.linspace(left, right, steps)
    plots = []
    plt.title("The function to differentiate numerically and"
              "its first and second derivative")
    plots.append(("f", frompyfunc11(finite_difference.func)(x_s)))
    plots.append(("d_f", frompyfunc11(finite_difference.compute_dh_f())(x_s)))
    plots.append(("dd_f",
                  frompyfunc11(finite_difference.compute_ddh_f())(x_s)))
    if finite_difference.d_func is not None:
        plots.append(("first derivative analytically",
                      frompyfunc11(finite_difference.d_func)(x_s)))
    if finite_difference.dd_func is not None:
        plots.append(("second derivative analytically",
                      frompyfunc11(finite_difference.dd_func)(x_s)))
    for plot in plots:
        plt.plot(x_s, plot[1], label=plot[0])
    plt.legend()


# Test function and their derivatives
def sinc(x: float) -> float:  # pylint: disable=invalid-name
    """
    sinc function

    :param x: Argument of sinc(x) (float)
    :return: sinc(x) = sin(x) / x (float)
    :raises ZeroDivisionError: if x is zero
    """
    return np.sin(x) / x


def d_sinc(x: float) -> float:  # pylint: disable=invalid-name
    """
    First derivative of sinc

    :param x: The point of interest (float)
    :return: The first derivative of sinc at x (float)
    """
    return frac(np.cos(x) * x - np.sin(x), x**2)


def dd_sinc(x: float) -> float:  # pylint: disable=invalid-name
    """
    Second derivative of sinc

    :param x: The point of interest (float)
    :return: The second derivative of sinc at x (float)
    """
    return frac(-np.sin(x), x) -\
        frac(2 * np.cos(x), x**2) +\
        2 * frac(np.sin(x), x**3)


def main():
    """
    Showing of all functionality of FiniteDifference and
    both plotting functions
    """
    left = np.pi
    right = 3 * np.pi
    steps = 1000
    stepwidth = frac(right - left, steps)

    finite_difference = FiniteDifference(stepwidth, sinc, d_sinc, dd_sinc)
    plot_functions(finite_difference, left, right, steps)
    plt.show()
    _, axes = plt.subplots()
    plot_errors(finite_difference, left, right, steps,
                np.linspace(stepwidth, 1, 10), axes)
    plt.show()


def plot_errors(finite_difference: FiniteDifference,
                # pylint: disable=too-many-arguments
                left: float, right: float, steps: int,
                stepwidths: List[float], axes: Axes) -> None:
    """
    A helper function plotting the errors and three potences of h
    of a given finite_difference object in an intervall [a, b] with
    p discretization steps

    :param finite_difference: error functions to plot (FiniteDifference)
    :param left: The left intervall boundary (float)
    :param right: The right intervall boundary (float)
    :param steps: The number of discretization steps (int)
    :return: None
    """
    func = finite_difference.func
    d_func = finite_difference.d_func
    dd_func = finite_difference.dd_func
    steps += 1
    e_1s = frompyfunc11(lambda h: FiniteDifference(h, func, d_func, dd_func).
            compute_errors(left, right, steps)[0])(stepwidths)
    e_2s = frompyfunc11(lambda h: FiniteDifference(h, func, d_func, dd_func).
            compute_errors(left, right, steps)[1])(stepwidths)
    h_1 = stepwidths**1
    h_2 = stepwidths**2
    h_3 = stepwidths**3

    axes.set_title("Error plots for h in [1, 10**(-50)]")
    axes.set_xlabel("Stepwidth h")
    axes.set_ylabel("Errorfunctions")
    axes.loglog(stepwidths, e_1s, label="e_1")
    axes.loglog(stepwidths, e_2s, label="e_2")
    axes.loglog(stepwidths, h_1, "--", label="h")
    axes.loglog(stepwidths, h_2, "--", label="h**2")
    axes.loglog(stepwidths, h_3, "--", label="h**3")
    axes.legend()
    axes.grid(True, which="both", ls="-", color='0.65')


if __name__ == "__main__":
    main()
