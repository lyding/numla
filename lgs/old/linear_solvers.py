"""
Project: Projektpraktikum I WiSe 22/23
Title: linear_solvers.py
Author: L. Brumm, L. Kunath
Date: 08/12/22

This Module contains the function solve_lu
to solve a linear equation system after an LU decomposition
"""
import numpy as np


def solve_lu(permutation, lower, upper, vector):
    """
    Solves the linear system Ax = b via forward and backward substitiution,
    given the decomposition A = p * l * u

    :param permutation: The permutation matrix (numpy matrix)
    :param lower: The lower matrix (numpy matrix)
    :param upper: The upper matrix (numpy matrix)
    :param vector: The inhomogenity vector (numpy array)
    """
    # First solve Ly = Pb by forward substitution
    y_s = np.asarray(permutation @ vector)
    for i in range(0, len(y_s)):  # pylint: disable=C0200
        for j in range(0, i):  # pylint: disable=C0200
            y_s[i] -= lower[i, j] * y_s[j]
    # Now solve Ux = y by backwards substituion
    for i in range(1, len(y_s) + 1):  # pylint: disable=C0200
        for j in range(1, i):  # pylint: disable=C0200
            y_s[-i] -= upper[-i, -j] * y_s[-j]
        y_s[-i] /= upper[-i, -i]
    return y_s


if __name__ == "__main__":
    import scipy as sc

    A = np.matrix([[1, 2, 4], [-1, 1, 0], [1, 0, -1]])
    b = np.array([1, 1, 1], dtype=float)
    P, L, U = sc.linalg.lu(A)  # pylint: disable=W0632
    print(sc.linalg.solve(A, b))
    print(solve_lu(P, L, U, b))
