"""
Project: Projektpraktikum I WiSe 22/23
Title: linear_solvers.py
Author: L. Brumm, L. Kunath
Date: 13/12/22

This module contains all code required to reproduce the results of
our numerical experiments
"""
import numpy as np
import poisson_problem as pp
import block_matrix as bm

# invalid name for the mathematical inclined variable names
# we considered using enumerate
# pylint: disable=invalid-name, consider-using-enumerate


def analytical_solution(position, kappa=1.0):
    """
    Calculates the value of the analytical solution
    at a given position

    :param position: The argument of the function as a numpy array
                     of length 1, 2 or 3
    :param kappa: An optional integer parameter to the function
    :return: The float value of the analytical solution at $position
    """
    result = 1
    for component in position:
        result *= component * np.sin(kappa * np.pi * component)
    return result


def inhomogenity(position, kappa=1.0):
    """
    Calculates the value of the inhomogenity
    at a given position

    :param position: The argument of the function as a numpy array
                     of length 1, 2 or 3
    :param kappa: An optional integer parameter to the function
    :return: The float value of the inhomogenity at $position
    """
    if len(position) == 1:
        x = position[0]  # pylint: disable=C0103
        return 2 * kappa * np.pi * np.cos(kappa * np.pi * x) -\
            kappa**2 * np.pi**2 * x * np.sin(kappa * np.pi * x)
    if len(position) == 2:
        x = position[0]  # pylint: disable=C0103
        y = position[1]  # pylint: disable=C0103
        ddx = 2 * kappa * np.pi * np.cos(kappa * np.pi * x) -\
            kappa**2 * np.pi**2 * x * np.sin(kappa * np.pi * x)
        ddy = 2 * kappa * np.pi * np.cos(kappa * np.pi * y) -\
            kappa**2 * np.pi**2 * y * np.sin(kappa * np.pi * y)
        return \
            ddx * y * np.sin(kappa * np.pi * y) + \
            ddy * x * np.sin(kappa * np.pi * x)
    if len(position) == 3:
        x = position[0]  # pylint: disable=C0103
        y = position[1]  # pylint: disable=C0103
        z = position[2]  # pylint: disable=C0103
        w = kappa * np.pi  # pylint: disable=C0103
        ddx = 2 * w * np.cos(w * x) -\
            kappa**2 * np.pi**2 * x * np.sin(w * x)
        ddy = 2 * w * np.cos(w * y) -\
            kappa**2 * np.pi**2 * y * np.sin(w * y)
        ddz = 2 * w * np.cos(w * z) - \
            kappa**2 * np.pi**2 * z * np.sin(w * z)
        return \
            ddx * y * np.sin(w * y) * z * np.sin(w * z) +\
            ddy * x * np.sin(w * x) * z * np.sin(w * z) +\
            ddz * x * np.sin(w * x) * y * np.sin(w * y)
    raise ValueError("position has to be a 1-, 2- or 3-dimensional vector")


def plot_condition():
    """
    Plot the condition of the discretized laplacians and the hilbert
    matrices as a reference
    """
    _, axes = plt.subplots(1)
    bm.plot_condition(axes, 1, list(range(2, 20)), True)
    bm.plot_condition(axes, 2, list(range(2, 20)), False)
    bm.plot_condition(axes, 3, list(range(2, 20)), False)
    plt.show()


def plot_poisson_solution():
    """
    Plot the solution of poissonproblem for dimensions 1 and 2 for
    different stepsizes
    """
    figure, axes = plt.subplots(2, 2)
    figure.suptitle("Solution of poisson problem for dimension 1")
    figure.tight_layout(pad=2.5)

    discretisation = np.linspace(0, 1, 1024)
    solution_array = np.zeros(len(discretisation))
    for i in range(len(discretisation)):
        solution_array[i] = analytical_solution([i / len(discretisation)])

    for i in range(4):
        n = 2 ** (i + 2)
        X = np.linspace(0, 1, n + 1)
        poisson_solution = pp.solve_poisson_problem(1, n, inhomogenity)
        error = round(pp.compute_error(1, n, poisson_solution,
            analytical_solution), 5)

        poisson_solution = np.pad(poisson_solution, (1, 1),
                                  'constant', constant_values=0)
        pic_now = [(0, 0), (0, 1), (1, 0), (1, 1)]
        axes[pic_now[i]].plot(X, poisson_solution, label="numerical solution")
        axes[pic_now[i]].plot(discretisation, solution_array, "--",
                              label="analytical solution")
        axes[pic_now[i]].set_title(f"n={2**(i+2)}, error={error}")
        axes[pic_now[i]].legend()
    plt.show()


def plot_2d_case():
    """
    Plot the numerical solution of the 2d case
    """
    figure, axes = plt.subplots(2, 2, subplot_kw={"projection": "3d"})
    figure.suptitle("Comparison of numerical and analytical solutions")
    for y in range(0, 2):
        for x in range(0, 2):
            n = 4 * (x + 2 * y + 1)
            N = n - 1
            intervals = np.linspace(0, 1, n + 1)
            X, Y = np.meshgrid(intervals, intervals)
            numerical_solution_n4 = np.array(pp.solve_poisson_problem(
                2, n, inhomogenity))
            error = pp.compute_error(2, n,
                                     numerical_solution_n4,
                                     analytical_solution)
            numerical_solution_n4 = numerical_solution_n4.reshape((N, N))
            numerical_solution_n4 = np.pad(numerical_solution_n4,
                                           (1, 1),
                                           'constant',
                                           constant_values=0)
            Z = np.zeros(N**2)
            for i in range(N**2):
                Z[i] = analytical_solution(np.array(pp.inv_idx(i + 1, 2, n))
                                           / n)
            Z = Z.reshape((N, N))
            Z = np.pad(Z, (1, 1), 'constant', constant_values=0)
            axes[y, x].set_title(f"n={n}, error={round(error, 5)}")
            surf = axes[y, x].plot_surface(X, Y, Z,
                                    cmap=cm.coolwarm,
                                    label="analytical solution")
            # This hack is needed to have a legend in a 2d plot
            # pylint: disable=protected-access
            surf._edgecolors2d = surf._edgecolor3d
            surf._facecolors2d = surf._facecolor3d
            axes[y, x].plot_wireframe(X, Y, numerical_solution_n4,
                                      color="black", linewidth=0.5,
                                      label="numerical solution")
            axes[y, x].legend()
    plt.show()


def plot_errors():
    """
    Plot the convergence of the numerical error for all three
    dimensionalities
    """
    figure, axis = plt.subplots(1)
    figure.suptitle("The error convergence for different dimensionalities")
    ns = list(range(2, 21))
    axis.set_xlabel("N")
    pp.plot_errors(1, list(range(2, 400)),
                   inhomogenity, analytical_solution, axis)
    pp.plot_errors(2, ns,
                   inhomogenity, analytical_solution, axis)
    pp.plot_errors(3, ns,
                   inhomogenity, analytical_solution, axis)
    xs = np.array(list(range(1, 1000)))
    axis.plot(xs, 1 / xs**(1 / 2), ls='--', label="N**(-1/2)")
    axis.plot(xs, 1 / xs**1, ls='--', label="N**(-1)")
    axis.plot(xs, 1 / xs**2, ls='--', label="N**(-2)")
    axis.grid(True, which="both", ls="-", color="0.65")
    axis.legend()
    plt.show()


def plot_sparsity():
    """
    Plot the sparsity of the laplacian matrix and its LU-decomposition
    """
    figure, axes = plt.subplots(3)
    figure.suptitle("Total Count of nonzero elements")
    bm.plot_number_of_not_zeros(axes[0], 1, list(range(2, 1000)), "12")
    bm.plot_number_of_not_zeros(axes[1], 2, list(range(2, 34)), "12")
    bm.plot_number_of_not_zeros(axes[2], 3, list(range(2, 20)), "12")
    plt.show()

    figure, axes = plt.subplots(1)
    figure.suptitle("Relative count of nonzero elements combined plot")
    bm.plot_number_of_not_zeros(axes, 1,
        list(range(2, 1000)), absolute=False)
    bm.plot_number_of_not_zeros(axes, 2,
        list(range(2, 34)), absolute=False)
    bm.plot_number_of_not_zeros(axes, 3,
        list(range(2, 20)), absolute=False)
    axes.loglog(list(range(1, 100)), [0.5 for x in range(1, 100)], "--")
    plt.show()


def plot_error_and_condition():
    """
    Plot the error and condition of the poisson problem
    """
    figure, axis = plt.subplots(1)
    figure.suptitle("Error and Condition as Functions of N")

    colors = ['red', 'green', 'blue']

    for d in [1, 2, 3]:
        ns = list(range(2, 15))

        errors = np.zeros(len(ns))
        conditions = np.zeros(len(ns))
        for i in range(len(errors)):
            n = ns[i]
            block_matrix = bm.BlockMatrix(d, n)
            _ = block_matrix.get_sparse()
            numerical_solution = np.array(pp.solve_poisson_problem(d,
                n, inhomogenity))
            errors[i] = pp.compute_error(d, n,
                numerical_solution, analytical_solution)
            conditions[i] = block_matrix.get_cond()
        # It is requested to plot the errors w.r.t N which is defined
        # as (n - 1)**d
        inner_pointss = (np.array(ns) - 1) ** d
        axis.loglog(inner_pointss, errors, label=f"error d={d}",
                    color=colors[d - 1], ls="--")
        axis.loglog(inner_pointss, conditions, label=f"condition d={d}",
                    color=colors[d - 1], ls="-")
        axis.loglog(inner_pointss, errors * conditions,
                    label=f"error * condition d={d}", ls=":",
                    color=colors[d - 1], linewidth=2.5)
    axis.set_xlabel("N")
    axis.grid(True, which="both", ls="-", color="0.65")
    axis.legend()
    plt.show()


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from matplotlib import cm

    while True:
        try:
            print("Numerical Experiments of Serie3 'LGS with LU': ")
            print("-----------------------------------------------")
            print("0. Plot 2d numerical and analytical solution "
                  "(n in [4, 8, 12, 16])")
            print("1. Plot error of numerical solution (may take few minutes")
            print("2. Plot Discretized Laplacian Matrix Conditioning Number")
            print("3. Plot Sparsity of discretized Laplacian Matrix and "
                  "their\n\tLU-Decomposition")
            print("4. Plot 1d numerical solutions")
            print("5. Plot error and condition (d = 2)")
            choice = int(input("Which plot should be produced: "))
            if choice not in [0, 1, 2, 3, 4, 5]:
                raise ValueError
            break
        except ValueError:
            # Print a newline for better readability only
            print("")
    [plot_2d_case, plot_errors, plot_condition,
     plot_sparsity, plot_poisson_solution, plot_error_and_condition][choice]()
