# Poissongleichung in der Physik

Die Poissongleichung ist eine partielle Differentialgleichung die in
unterschiedlichen klassischen Feldtheorien auftaucht.
Der bekannteste Fall ist dabei die Elektrostatik in welcher sich
die Poissongleichung aus dem Divergenz-Gesetz der elektrischen
Felder und dem, dem elektrischen Feld zugrunde liegende Potential ergibt.

Der physikalische Gauszsatz setzt die Divergenz des elektrischen Feldes
und der Ladungsdichte in Beziehung:

\[
\nabla E(r) = \frac{\rho(r), \epsilon_0}
\]
wobei \(E:\mathbb{R}^3\rightarrow\mathbb{R}^3\) das elektrische
Feld, \(\rho:\mathbb{R}^3\mathbb{R}\) die Ladungsdichte und \(epsilon_0\)
die elektrische Feldkonstante darstellt.

Da weiterhin
\[
\nabla\cross E(r) = 0
\]
gilt, das elektrische Feld also ein konservatives Feld ist,
existiert ein, bis auf Addition einer Konstante, eindeutiges
Potentialfeld \(\phi:\mathbb{R}^3\rightarrow\mathbb{R}\) sodass
\[
E(r) = -\nabla\phi(r)
\]
gilt. Wendet man nun den Nabla-Operator erneut auf an,
so folgt sofort die Poissongleichung der Elektrostatik

\[
\nabla E(r) = \frac{\rho(r)}{\epsilon_0} = -\bigtriangleup\phi(r)
\]

Da der Gravitation ein strukturell gleiches Kraftgesetz zugrunde liegt
\[
F = G\frac{m_1m_2}{r^2}
\]
folgt mit degm Gauszgesetz der Gravitation \(\nabla g(r) = -4\pi G\rho_m(r)\),
wobei nun \(g:mathbb{R}\rightarrow\mathbb{R}^3\) das Gravitationsfeld, \(\rho(r):\mathbb{R}\rightarrow\mathbb{R}\)
die Massedichte und \(G\) die Gravitationskonstante darstell,
und mit einer analogen Argumentation die Poissonleichung der Gravitation:
\[
\bigtriangleup\phi(r) = 4\pi G\rho(r)
\]

# Randbedingungen
Waehrend Differentialgleichungen genutzt werden koennen um natuerlich vorkommende oder menschengemachte Prozesse
mathematisch zu modellieren sind es ihre Loesungen die die Entwicklung des modellierten Systems beschreiben.
Diese Loesungen sind erst durch gestellte Randbedingungen eindeutig bestimmt. Dementsprechend habe die
Randbedingungen einen entscheidenden Einfluss auf die Loesung der Differentialgleichung. Loest man Differentialgleichungen
numerisch ergeben sich aus der Begrenztheit des Diskretisierungsgitters sog. numerische Randbedingungen, die ebenfalls
die gefundene Loesung beeinflussen koennen.
In diesem Abschnitt sollen zumindestens die Dirichlet- und die Neumann-Randbedingung genannt werden. (Diese
Liste kann aber natuerlich noch verlaengert werden)

## Numerische Randbedingung
Da Speicherplat- und Rechenkapazitaeten begrenzt sind koennen am Rechner nur endliche Gebiete diskretisiert werden.
Fuer Punkte die im Inneren des Diskretisierungsgitters liegen kann die Ableitung durch die finite Differenzenmethode
approximiert werden. Das ist am Rand des Diskretisierungsgitters nicht moeglich. Die naheliegendste Art damit
umzugehen ist anzunehmen, dass der Funktionswert an Punkte die auszerhalb des Diskretisierungsgitters laegen
identisch $0$ ist. Das entspricht gerade einem Spezialfall der Dirichlet-Randbedingung.

## Dirichlet Randbedingung
Die allgemeine Dirichlet Randbedingung setzt die zu suchende Funktion $u$ nur auf eine allgemeine Funktion $g$.
\[
-\bigtriangleup u = f\quad\text{in}\,\Omega\\
-\bigtriangleup u = g\quad\text{in}\,\partial\Omega
\]
Die numerische Randbedingung (siehe Numerische Randbedingung) entspricht dem Spezialfall, $g = 0$.

## Neumann Randbedingung
Die Neumann Randbedingung setzt die Ableitung der zu suchenden Funktion $u$ auf dem Rand des Definitionsbereichs fest
\[
\partial_n / \partial_n r u(r) = g(x), x \in \partial\Omega
\]
Hierbei bezeichnet $\partial_n / \parital_n r$ die Richtungsableitung entlang der Normalen auf $\partial \Omega$

## Absorbing Boundary Conditions (ABCs)
Da Randbedingungen die Loesung der Differentialgleichung eindeutig definieren (siehe: Eindeutigkeitssatz)
koennen die numerischen Randbedingungen die gesuchte Loesung verfaelschen, oder aber das Finden der gewuenschten
Loesung unmoeglich machen. Es soll beispielhaft die Situation betrachtet werden, dass die Ausbreitung
elektromagnetischer Wellen ueber einen langen Zeitraum simuliert werden soll um die Abstrahlcharakteristik
einer Antenne zu ermitteln. Wird hierbei die numerische Randbedingung durch den Spezialfall der Dirichlet-Randbedingung
geloest, d.h. wird das elektrische Potential am Rand des Diskretisierungsgitters auf $0$ gesetzt, so wird
die abgestrahlte EM-Welle am Rand des Diskretisierungsgitters total reflektieren und mit dem Nahfeld der Antenne
inteferieren. Das macht es unmoeglich die Abstrahlcharakteristik der Antenne korrekt zu ermitteln.
Die naive Loesung, das Diskretisierungsgitte groeszer zu waehlen bietet sich aufgrund der rasch anwachsenden Komplexitaet
nicht an. Absorbing Boundary Conditions ist eine Klasse an Verfahren mit denen man versucht die Funktion nahe des
Randes des Diskretisierungsitters stetig gegen $0$ abzusenken ohne dabei einen stoerenden Einfluss auf die Funktion
an anderen Stellen des Diskretisierungsgitters auszuueben. Damit kann die numerische Randbedingung weiterhin
mit dem Spezialfall der Dirichlet-Randbedingung geloest werden, da die Funktion am Rand des Diskretisierungsgitters
schon auf $0$ abgefallen ist. Bekannte ABCs fuer den oben beschriebenen Fall der elektromagnetischen Welle
waeren 'Mur Absorbing Boundary Condition' oder 'Perfectly Matched Layer (PML)' obwohl es sich hierbei um eine absorbierende
Region und nicht eine ABC im strengen Sinne handelt. Dennoch wird die PML meist zu den ABCs gezaehlt.
