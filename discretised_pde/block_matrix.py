"""

Author: L. Brumm
Date: 18.011.22
"""

import numpy as np
import scipy.sparse as sp
from functools import reduce


class BlockMatrix:
    """ Represents block matrices arising from finite difference approximations
    of the Laplace operator.

    Parameters
    ----------
    dimension : int
        Dimension of the space.
    steps : int
        Number of intervals in each dimension.

    Attributes
    ----------
    d : int
        Dimension of the space.
    n : int
        Number of intervals in each dimension.

    Raises
    ------
    ValueError
        If d < 1 or n < 2.
    """

    def __init__(self, dimension: int, steps: int):
        self.dimension = dimension
        self.steps = steps
        self.matrix = None

    def get_sparse(self, fmt="coo"):
        """
        Constructs the block matrix as sparse matrix.

        :return: The constructed sparse matrix
        """

        A = sp.diags([[2] * self.steps,
                      [-1] * self.steps,
                      [-1] * self.steps], [0, -1, 1], format=fmt)
        for loop_index in range(2, self.dimension + 1):
            identity_submatrix = sp.eye(self.steps**(loop_index - 1))
            matrix_grid = np.array([None] * self.steps**2).\
                reshape((self.steps, self.steps))
            np.fill_diagonal(matrix_grid, A)
            # I could not find a nice way to set the secondary diagonals
            # So a loop is used...
            for i in range(0, self.steps - 1):
                matrix_grid[i + 1, i] = identity_submatrix
                matrix_grid[i, i + 1] = identity_submatrix
            A = sp.bmat(matrix_grid)
        self.matrix = A
        return A

    def eval_sparsity(self):
        """
        Returns the absolute and relative numbers of non-zero elements of
        the matrix. The relative quantities are with respect to the total
        number of elements of the represented matrix.
        :return: A tuple (integer, float) storing the absolute and relative
                 number of non-zero elements
        :raises ValueError: If the sparse matrix was not yet constructed
        """
        if self.matrix is None:
            raise ValueError("sparse matrix is None, "
                             "did you forget to call 'get_sparse'?")
        nonzero = self.matrix.count_nonzero()
        return nonzero,\
            nonzero / reduce(lambda x, y: x * y, self.matrix.shape, 1)


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    D = BlockMatrix(2, 20)
    A = D.get_sparse()
    print(D.eval_sparsity())
    plt.spy(A)
    plt.show()
