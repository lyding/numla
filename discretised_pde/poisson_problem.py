from typing import List


def rhs(d, n, f):
    """ Computes the right-hand side vector `b` for a given function `f`.

    Parameters
    ----------
    d : int
        Dimension of the space.
    n : int
        Number of intervals in each dimension.
    f : callable
        Function right-hand-side of Poisson problem. The calling signature is
        `f(x)`. Here `x` is an array_like of `numpy`. The return value
        is a scalar.

    Returns
    -------
    numpy.ndarray
        Vector to the right-hand-side f.

    Raises
    ------
    ValueError
        If d < 1 or n < 2.
    """


def idx(coord: List[int], steps: int) -> int:
    """
    Calculates the number of an equation in the Poisson problem for
    a given discretization point.

    :param coord: The coordinate of the discretization point multiplied by n
    :param steps: The number of discretization steps in each dimension
    :return: The index in the poisson problem
    """
    result = 0
    for i in range(len(coord)):
        result += coord[i] * steps**i
    return result


def inv_idx(index: int, dimensions: int, steps: int) -> int:
    """
    Calculates the coordinates of a discretization point for a
    given equation number of the Poisson problem.

    :param index: The index of the equation in the poisson problem
    :param dimensions: Number of dimensions
    :param steps: The number of discretization steps in each dimension
    :return: A list of integer containing the coords of the discretization
             point
    """
    coords = [0 for x in range(dimensions)]
    for dimension in range(dimensions):
        coords[dimensions - dimension - 1] = index //\
            steps**(dimensions - dimension - 1)
        index = index - coords[dimensions - dimension - 1] *\
            steps**(dimensions - dimension - 1)
    return coords


if __name__ == "__main__":
    print(inv_idx(idx([1, 2, 3], 4), 3, 4))
