"""
Project: Projektpraktikum I WiSe 22/23
Title: exercises.py
Author: L. Brumm, L. Kunath
Date: 30/11/22

In this programme the methode of finite differences is used
to compute the first and second derivative of a function and
we will exame its convergence behaviour as a function of the step width
of the finite differences
"""
import numpy as np
import matplotlib.pyplot as plt
from derivative_approximation import (
    FiniteDifference,
    sinc, d_sinc, dd_sinc,
    frompyfunc11,
    plot_errors,
    main)

# It would look rather ugly to put it everywhere this warning occurs
# pylint: disable=cell-var-from-loop:


def exercise1_2():
    """
    Compare the numerical and analytical first and second derivative of
    the sinc function, for various h parameters
    """
    left = np.pi
    right = 3 * np.pi
    steps = 1000
    figure = plt.figure(1)
    figure.suptitle("First and Second Numerical and "
                    "Analytical Differentiations")
    denoms = [3, 4, 5, 10]
    for i in range(4):
        d_axis = figure.add_subplot(2, 4, i + 1)
        d_axis.set_title(f"h = pi / {denoms[i]}")
        d_axis.grid()
        finite_difference = FiniteDifference(np.pi / denoms[i],
                                             sinc, d_sinc, dd_sinc)
        x_s = np.linspace(left, right, steps)

        d_axis.plot(x_s,
                    frompyfunc11(finite_difference.compute_dh_f())(x_s),
                    "--",
                    label="dh_f")
        d_axis.plot(x_s,
                    frompyfunc11(finite_difference.d_func)(x_s),
                    label="d_f")
        d_axis.legend()

        dd_axis = figure.add_subplot(2, 4, i + 5)
        dd_axis.grid()
        dd_axis.plot(x_s,
                    frompyfunc11(finite_difference.compute_ddh_f())(x_s),
                    "--",
                    label="ddh_f")
        dd_axis.plot(x_s,
                    frompyfunc11(finite_difference.dd_func)(x_s),
                    label="dd_f")
        dd_axis.legend()
    plt.show()

    exp = 20
    _, axes = plt.subplots()
    axes.set_xlabel("Stepwidth h")
    axes.set_ylabel("Errorfunctions")
    stepwidths = frompyfunc11(lambda x: 1 / 10**x)(np.linspace(1, exp, exp))
    plot_errors(finite_difference, left, right, steps, stepwidths, axes)
    plt.show()


def exercise1_3():  # pylint: disable=too-many-locals
    """
    Compare the error convergence behaviour of the finite differerence
    approximation for sin(kx) / x
    """
    exp = 20
    left = np.pi
    right = 3 * np.pi
    steps = 1000
    figure, axes = plt.subplots(2, 1)
    figure.suptitle("Comparison of error convergence behaviour"
                    "for different frequencies k")
    axes[0].set_xlabel("Stepwidth h")
    axes[0].set_ylabel("Errorfunction of the first derivative")
    stepwidths = frompyfunc11(lambda x: 1 / 10**x)(np.linspace(1, exp, exp))
    for k in [0.0001, 0.1, 1, 10]:
        def g_k(x):  # pylint: disable=invalid-name
            return np.sin(k * x) / x

        def d_g_k(x):  # pylint: disable=invalid-name
            return (k * np.cos(k * x) * x - np.sin(k * x)) / x**2

        def dd_g_k(x):  # pylint: disable=invalid-name
            return (-k**2 * np.sin(k * x)) / x - 2 * ((k * np.cos(k * x))
                    / x**2 - np.sin(k * x) / x**3)
        e_1s = frompyfunc11(lambda h: FiniteDifference(h, g_k, d_g_k, dd_g_k).
                compute_errors(left, right, steps)[0])(stepwidths)
        axes[0].loglog(stepwidths, e_1s, label=f"k = {k}")
    axes[0].legend()
    axes[0].grid(True, which="both", ls="-", color='0.65')
    axes[1].set_xlabel("Stepwidth h")
    axes[1].set_ylabel("Errorfunction of the second derivative")
    stepwidths = frompyfunc11(lambda x: 1 / 10**x)(np.linspace(1, exp, exp))
    for k in [0.0001, 0.1, 1, 10]:
        def g_k1(x):  # pylint: disable=invalid-name
            return np.sin(k * x) / x

        def d_g_k1(x):  # pylint: disable=invalid-name
            return (k * np.cos(k * x) * x - np.sin(k * x)) / x**2

        def dd_g_k1(x):  # pylint: disable=invalid-name
            return (-k**2 * np.sin(k * x)) / x - 2 * ((k * np.cos(k * x))
                    / x**2 - np.sin(k * x) / x**3)
        e_2s = frompyfunc11(lambda h: FiniteDifference(h, g_k1,
                                                       d_g_k1, dd_g_k1).
            compute_errors(left, right, steps)[1])(stepwidths)
        axes[1].loglog(stepwidths, e_2s, label=f"k = {k}")
    axes[1].grid(True, which="both", ls="-", color='0.65')
    axes[1].legend()
    plt.show()


if __name__ == "__main__":
    # Loop as long as the users input is invalid
    while True:
        try:
            choice = int(input(
                "Type (0) for main, (1) for experiment_1.2 "
                "or (2) for experiment_1.3: "))
            if choice not in [0, 1, 2]:
                raise ValueError
            break
        except ValueError:
            pass
    [main, exercise1_2, exercise1_3][choice]()
